const fs = require('fs')
const yb = require('./yb.json')

yb.forEach(y=>{
    let n = encodeURIComponent(y);
    if(n==y) return;
    console.info('rename',y);
    fs.renameSync(`./mp3/${n}.mp3`,`./mp3/${y}.mp3`);
})

