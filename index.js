

const yb = ["iː", "ɜː", "ɑː", "ɔː", "uː", "ɪ", "e", "æ", "ə", "ʌ", "ɒ", "ʊ", "eɪ", "aɪ", "ɔɪ", "əʊ", "aʊ", "ɪə", "eə", "ʊə", "p", "t", "k", "f", "θ", "s", "ʃ", "h", "tʃ", "ts", "tr", "b", "d", "ɡ", "v", "ð", "z", "ʒ", "r", "dʒ", "dz", "dr", "m", "n", "ŋ", "l", "j", "w"];

const player = document.getElementById('mp3play')

yb.forEach(y => {
    var a = document.createElement('link');
    a.rel = "prefetch";
    a.href = `./mp3/${y}.mp3`;
    document.head.appendChild(a);
});

function init(yb) {
    let html = yb.map(y => `<card>${y}</card>`)
    document.getElementById('cards').innerHTML = html.join('');
}

init(yb);

document.addEventListener('click', e => {
    if (e.target.tagName != 'CARD') return;
    doPlay(e.target,1);
})

player.addEventListener("ended",async e => {
    console.timeEnd("play");
    console.info(player.playCount);
    let count = +player.playCount;
    count--;
    player.playCount = count;
    if (count > 0) {
        await sleep(500);
        console.time("play");
        player.play();
    }
})

function doPlay(card,count=1) {
    document.querySelector(".active")?.classList.remove("active");
    card.classList.add("active")
    player.playCount = count;
    player.src = `./mp3/${(card.innerText)}.mp3`;
    console.time("play");
    player.play();
}

function rnd() {
    let t = [...yb];
    for (let index = 0; index < 3; index++) {
        t.sort(_ => Math.random() - 0.5);
    }
    init(t);
}

function next() {
    let active = document.querySelector('.active');
    if (!active) return doPlay(document.querySelector("card"),document.getElementById('playCount').value);
    let n = active.nextElementSibling;
    if (n) doPlay(n,document.getElementById('playCount').value);
}

function PlayNow() {
    let active = document.querySelector('.active');
    if (!active) return doPlay(document.querySelector("card"),document.getElementById('playCount').value);
    doPlay(active,document.getElementById('playCount').value);
}

let sleep = (time) => new Promise((resolve) => setTimeout(resolve, time));